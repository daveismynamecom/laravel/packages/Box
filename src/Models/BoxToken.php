<?php

namespace DaveismynameLaravel\Box\Models;

use Illuminate\Database\Eloquent\Model;

class BoxToken extends Model
{
    protected $guarded = [];
}
